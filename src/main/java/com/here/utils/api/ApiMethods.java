package com.here.utils.api;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.HashMap;

public class ApiMethods {

    private OkHttpClient client;
    private HttpHelper httpHelper;

    public ApiMethods(OkHttpClient okHttpClient) {
        this.client = okHttpClient;
        this.httpHelper = new HttpHelper();
    }

    public Response getCurrentWeather(String cityName) throws IOException {
        HashMap<String, String> params = new HashMap<>();
        params.put("q", cityName);
        Request getRequest = new Request.Builder()
                .url(httpHelper.getUrl(Endpoints.CURRENT_WEATHER, params))
                .get()
                .build();

        return client.newCall(getRequest).execute();
    }
}
