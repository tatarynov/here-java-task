package com.here.utils.api;

import okhttp3.HttpUrl;

import java.util.HashMap;

public class HttpHelper {

    private static final String BASE_URL = "api.openweathermap.org";
    private static final String API_VERSION = "2.5";
    private static final String API_KEY = "2129b0d4a034345575dc2dca4e05a2e6";

    public HttpUrl getUrl(String endpoint, HashMap<String, String> queryParams) {
        HttpUrl.Builder builder = new HttpUrl.Builder()
                .scheme("https")
                .host(BASE_URL)
                .addPathSegment("data")
                .addPathSegment(API_VERSION)
                .addPathSegment(endpoint);
        queryParams.forEach(builder::addQueryParameter);
        return builder
                .addQueryParameter("APPID", API_KEY)
                .build();
    }
}
