package com.here.utils;

import com.here.dto.City;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class YamlHelper {

    private final static String CITIES_YML = "/cities.yml";

    @SuppressWarnings("unchecked")
    public static ArrayList<City> getCitiesList() throws IOException {
        ArrayList<City> arrayList = new ArrayList<>();
        Yaml yaml = new Yaml();
        try (InputStream in = YamlHelper.class.getResourceAsStream(CITIES_YML)) {
            Iterable<Object> itr = yaml.loadAll(in);
            for (Object o : itr) {
                List cities = ((LinkedHashMap<String, List>) o).get("cities");
                for (Object city : cities) {
                    City c = new City().setName((String) city);
                    arrayList.add(c);
                }
            }
        }
        return arrayList;
    }
}
