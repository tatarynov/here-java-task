package com.here.utils;

import com.here.dto.Forecast;
import com.here.dto.SystemParameters;

import java.util.List;

public class ForecastHelper {

    public enum DayLight {SHORTEST, LONGEST}

    public Forecast getForecastWithDayLight(List<Forecast> forecastList, DayLight dayLight) {
        if (forecastList.isEmpty()) throw new IllegalArgumentException("Forecasts list is empty!");
        Forecast result = null;
        SystemParameters systemParameters = forecastList.get(0).getSystemParameters();

        long initValue = systemParameters.getSunset() - systemParameters.getSunrise();
        for (Forecast forecast : forecastList) {
            SystemParameters sysParams = forecast.getSystemParameters();
            long delta = sysParams.getSunset() - sysParams.getSunrise();
            if (dayLight.equals(DayLight.SHORTEST)) {
                if (initValue < delta) {
                    initValue = delta;
                    result = forecast;
                }
            } else if (dayLight.equals(DayLight.LONGEST)) {
                if (initValue > delta) {
                    initValue = delta;
                    result = forecast;
                }
            }
        }

        return result;
    }
}
