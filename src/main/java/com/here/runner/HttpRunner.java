package com.here.runner;

import com.google.gson.Gson;
import com.here.dto.City;
import com.here.dto.Forecast;
import com.here.utils.ForecastHelper;
import com.here.utils.YamlHelper;
import com.here.utils.api.ApiMethods;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class HttpRunner {

    private static final OkHttpClient CLIENT = new OkHttpClient()
            .newBuilder()
            .build();
    private static final ApiMethods API = new ApiMethods(CLIENT);
    private static final Logger LOG = LoggerFactory.getLogger(HttpRunner.class);
    private static final ForecastHelper FORECAST_HELPER = new ForecastHelper();

    public static void main(String[] args) throws IOException {
        LOG.info("Program started...");
        ArrayList<Forecast> forecasts = new ArrayList<>();
        ArrayList<City> citiesList = YamlHelper.getCitiesList();
        for (City city : citiesList) {
            LOG.info("Get current weather for {}", city.getName());
            String json = Objects.requireNonNull(API.getCurrentWeather(city.getName())
                    .body(), String.format("No response was received for %s", city.getName()))
                    .string();
            LOG.debug("Json is response is {}", json);
            LOG.debug("Deserializing json from response");
            Forecast forecast = new Gson().fromJson(json, Forecast.class);
            forecasts.add(forecast);
        }

        LOG.info("\n");
        LOG.info("Getting temperature for city with shortest daylight");
        Forecast shortest = FORECAST_HELPER
                .getForecastWithDayLight(forecasts, ForecastHelper.DayLight.SHORTEST);
        double shortestTemp = shortest
                .getMainParameters()
                .getTemperature();
        LOG.info("Temp for city with shortest daylight ({}) is {}\n", shortest.getName(), shortestTemp);

        LOG.info("Getting temperature for city with longest daylight");
        Forecast longest = FORECAST_HELPER
                .getForecastWithDayLight(forecasts, ForecastHelper.DayLight.LONGEST);
        double longestTemp = longest
                .getMainParameters()
                .getTemperature();
        LOG.info("Temp for city with longest daylight ({}) is {}\n", longest.getName(), longestTemp);

    }
}
