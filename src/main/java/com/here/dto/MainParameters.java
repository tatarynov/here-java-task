package com.here.dto;

import com.google.gson.annotations.SerializedName;

public class MainParameters {

    @SerializedName("temp")
    private double temperature;

    private long pressure;

    @SerializedName("temp_min")
    private double minTemp;

    @SerializedName("temp_max")
    private double maxTemp;

    public double getTemperature() {
        return temperature;
    }

    public long getPressure() {
        return pressure;
    }

    public double getMinTemp() {
        return minTemp;
    }

    public double getMaxTemp() {
        return maxTemp;
    }
}
