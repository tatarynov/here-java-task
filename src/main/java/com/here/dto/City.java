package com.here.dto;

public class City {

    private String name;

    public String getName() {
        return name;
    }

    public City setName(String name) {
        this.name = name;
        return this;
    }
}
