package com.here.dto;

import com.google.gson.annotations.SerializedName;

public class Forecast {

    @SerializedName("main")
    private MainParameters mainParameters;

    @SerializedName("sys")
    private SystemParameters systemParameters;

    private long id;

    private String name;

    public MainParameters getMainParameters() {
        return mainParameters;
    }

    public SystemParameters getSystemParameters() {
        return systemParameters;
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }
}
