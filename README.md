# Java Task - Open Weather API
## Tech stack:
- Java 8 ([Install docs](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html))
- Maven ([Install docs](http://maven.apache.org/install.html))


## Running the application
1. Clone project using command:
```
git clone https://tatarynov@bitbucket.org/tatarynov/here-java-task.git
```
2. Go to cloned project and run next command: 
```
mvn clean install exec:java
```